import { schemas } from './schema';
jest.mock('fs');
import * as fs from 'fs';

describe('Schemas', () => {
    beforeEach(() => {
        jest.spyOn(fs, 'readFileSync');
        schemas;
    });
    it('It should call fs with station schema', () => {
        expect(fs.readFileSync).toHaveBeenCalledWith('./src/server/schema/Station.graphql', 'utf-8');
    });
    it('It should call fs with sensor schema', () => {
        expect(fs.readFileSync).toHaveBeenCalledWith('./src/server/schema/Sensor.graphql', 'utf-8');
    });
    it('It should call fs with measure schema', () => {
        expect(fs.readFileSync).toHaveBeenCalledWith('./src/server/schema/Measure.graphql', 'utf-8');
    });
    it('It should call fs with root schema', () => {
        expect(fs.readFileSync).toHaveBeenCalledWith('./src/server/schema/Root.graphql', 'utf-8');
    });
    it('It should call fs with notification schema', () => {
        expect(fs.readFileSync).toHaveBeenCalledWith('./src/server/schema/Notification.graphql', 'utf-8');
    });
});
