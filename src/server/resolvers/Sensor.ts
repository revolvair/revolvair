import { Sensor } from '../../models/Sensor';
import { ObjectResolver, Resolver } from './Resolver';
import { TimeRangeArgs } from '../DBClient';


export class SensorResolver extends ObjectResolver implements Resolver {

    private sensorResolver = {
        Sensor: {
            latestMeasures: async (root: Sensor) => {
                return await this.getDbClient().getMeasureBySensorId(root.id) || [];
            },
            measures: async (root: Sensor, args: TimeRangeArgs) => {
                return await this.getDbClient().getMeasureByTimeOption(root.id, args) || [];
            },
        },
    };

    public getResolver() {
        return this.sensorResolver;
    }
}
