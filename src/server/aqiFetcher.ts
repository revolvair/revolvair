import axios from 'axios';
import {isDefined} from 'welshguard';
import * as dotenv from 'dotenv';
// import {useAuth0} from '../utils/hook';

dotenv.config();

const urlAqiQuebecStations = 'http://api.waqi.info/map/bounds/?latlng=46.845564,-71.200263,46.745673,-71.401107&token=' + process.env.AQI_TOKEN;
const urlAqiStation = 'http://api.waqi.info/feed/@$STATION_ID/?token=' + process.env.AQI_TOKEN;

const mutationAddStation: string = `
mutation {
    addAqiStations(name:"$CITY_NAME",userId:$USER_ID, long:$CITY_LONG, lat:$CITY_LAT, stationId:$STATION_ID) {
        name, userId, long, lat
    }
}`;

const mutationAddMeasure: string = `
mutation {
    addMeasure(stationId: $STATION_ID, sensorId: $SENSOR_ID, value: $VALUE) {
        sensorId, value
    }
}`;

const queryStationIds: string = `
{
    stations {
        id
    }
}`;

const SECOND_DIC_INDEX = 1;
let STATIONS_EXIST = false;


// const user = useAuth0();

export class AqiFetcher {

    async requestAqi() {
        const url = process.env.DB_URL + 'revolvairstations';

        await this.sleep(45000);

        axios.head(url).then((response) => {
            if (response.status == 404) {
                this.requestAqi();
            } else if (response.status == 200) {
                axios.get(urlAqiQuebecStations).then((response) => {
                    try {
                        this.verifyAqiStationsExist();
                        this.assignData(response.data);
                    } catch (e) {
                        console.log(e);
                    }
                }, (error) => {
                    console.log(error);
                });
            }
        }, (error) => {
            console.log(error);
        });
    }

    private async verifyAqiStationsExist() {
        // If these ids change than change the verification.
        const aqiStationIds = [5438, 5446, 5447, 5426, 5445];

        await axios({
            url: process.env.APP_LOCAL_GRAPHQL_URL,
            method: 'get',
            data : {
                query: queryStationIds,
            },
        }).then((result) => {
            for (let i = 0; i < result.data.stations; i++) {
                for (let n = 0; n < aqiStationIds.length; n++) {
                    if (result.data.stations[i].id == aqiStationIds[n]) {
                        STATIONS_EXIST = true;
                    }
                }
            }
        }, (error) => {
            console.log(error);
        });
    }

    private sleep(ms: number) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    private async assignData(jsonParsed: any) {
        const ANY_USER_ID = '15';

        const stations = jsonParsed.data;
        const stationsArray: number[][] = new Array();

        for (let i = 0; i < stations.length; i++) {
            const id = stations[i].uid;
            stationsArray.push([id, id]);
        }

        for (let i = 0; i < stations.length; i++) {
            let mutationAddStationCommand = mutationAddStation;
            const station = stations[i];

            const cityName = station.station.name;
            const stationId = station.uid;
            const lat = station.lat;
            const long = station.lon;

            mutationAddStationCommand = mutationAddStationCommand.replace('$CITY_NAME', cityName);
            mutationAddStationCommand = mutationAddStationCommand.replace('$USER_ID', ANY_USER_ID);
            mutationAddStationCommand = mutationAddStationCommand.replace('$CITY_LONG', long);
            mutationAddStationCommand = mutationAddStationCommand.replace('$CITY_LAT', lat);
            mutationAddStationCommand = mutationAddStationCommand.replace('$STATION_ID', stationId);

            // add stations
            // will need to add code for addSensorToStation for sensorTypes (1 | 2 | 3 | 4)
            if (!STATIONS_EXIST) {
                axios({
                    url: process.env.APP_LOCAL_GRAPHQL_URL,
                    method: 'post',
                    data : {
                        query: mutationAddStationCommand,
                    },
                }).then((result) => {
                    // console.log(result.data);
                });
            }

            // get the station measure
            const value = await this.getPM25Value(stationId);

            // add the station measure to graphql
            if(value != -1) {
                await this.addStationMeasures(stationId, stationsArray[i][SECOND_DIC_INDEX], value.toString());
            }
        }

        // let interval = 30000; // default value
        // if (isDefined(user.user_metadata.measureFrequency)) {
        //     interval = user.user_metadata.measureFrequency;
        // }
        setInterval(async () => {
            for (let i = 0; i < stations.length; i++) {
                const station = stations[i];
                const stationId = station.uid;
                const value = await this.getPM25Value(stationId);
                if(value != -1) {
                    await this.addStationMeasures(stationId, stationsArray[i][SECOND_DIC_INDEX], value.toString());
                }
            }
        }, 30000);
    }

    private async getPM25Value(stationId: string) {
        let stationUrl = urlAqiStation;
        stationUrl = stationUrl.replace('$STATION_ID', stationId);

        let value;
        await axios.get(stationUrl).then((response) => {
            if(isDefined(response.data.data.iaqi.pm25)){
                value = response.data.data.iaqi.pm25.v; // get pm25 value directly on the json
            }
        }, (error) => {
            console.log(error);
        });

        if (isDefined(value)) {
            return value;
        } else {
            return -1;
        }
    }

    private async addStationMeasures(stationId: string, sensorId: number, value: string) {
        let mutationAddMeasureCommand = mutationAddMeasure;
        mutationAddMeasureCommand = mutationAddMeasureCommand.replace('$STATION_ID', stationId);
        mutationAddMeasureCommand = mutationAddMeasureCommand.replace('$SENSOR_ID', sensorId.toString());
        mutationAddMeasureCommand = mutationAddMeasureCommand.replace('$VALUE', value);

        const token = String(process.env.REACT_APP_TOKEN);

        await axios({
            url: process.env.APP_LOCAL_GRAPHQL_URL,
            method: 'post',
            headers: {
                'Authorization': `Bearer ${token}`,
            },
            data : {
                query: mutationAddMeasureCommand,
            },
        }).then((result) => {
            console.log(result.data);
        }, (error) => {
            console.log(error);
        });
    }
}
