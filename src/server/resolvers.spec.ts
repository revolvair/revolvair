import { resolvers } from './resolvers';

const sizeOfOne = 1;
const sizeOfTwo = 2;
const sizeOfThree = 3;

describe('List of resolvers', () => {
    it('It should contain Station\'s resolver', () => {
        expect(resolvers.length).toBeGreaterThanOrEqual(sizeOfOne);
    });

    it('It should contain Sensor\'s resolver', () => {
        expect(resolvers.length).toBeGreaterThanOrEqual(sizeOfTwo);
    });

    it('It should contain Mutation\'s resolver', () => {
        expect(resolvers.length).toBeGreaterThanOrEqual(sizeOfThree);
    });
});
