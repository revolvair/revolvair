import {Station} from '../models/Station';
import {Sensor} from '../models/Sensor';
import {Measure} from '../models/Measure';
import {Notification} from '../models/Notification';


export interface TimeRangeArgs {
    timeRange: string;
}

export interface DBClient {
    // GET
    getAllStations(): Promise<Station[] | void>;
    getAllStationById(id: number): Promise<Station[] | void>;
    getSensorsIdsByStationId(id: number): Promise<Sensor[] | void>;
    getSensorIdByStationIdAndSensorId(stationId: number, sensorId: number): Promise<Sensor[] | void>;
    getMeasureBySensorId(sensorId: number): Promise<Measure[] | void>;
    getMeasureByTimeOption(sensorId: number, timesRangeArgs: TimeRangeArgs): Promise<Measure[] | void>;
    getNotificationByStationId(stationId: number): Promise<Notification[] | void>;

    // ADD
    addStation(name: string, userId: number, long: number, lat: number): Promise<Station | void>;
    addSensorToStation(sensorId: number, sensorType: number, stationId: number): Promise<Station | void>;
    addMeasure(stationId: number, sensorId: number, value: number): Promise<Measure | void>;
    addNotification(stationId: number, oldValue: number, newValue: number, oldState: string, newState: string): Promise<void>;

    addAqiStations(name: string, userId: number, long: number, lat: number, stationId: number): Promise<Station | void>;
}
