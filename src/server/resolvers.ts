import { StationResolver } from './resolvers/Station';
import { SensorResolver } from './resolvers/Sensor';
import { Mutations } from './resolvers/Mutations';
import { elasticClient, ElasticClientDB} from './elastic';

const dbClient = new ElasticClientDB(elasticClient);

const stationResolver = new StationResolver(dbClient);
const sensorResolver = new SensorResolver(dbClient);
const measureResolver = new Mutations(dbClient);

export const resolvers = [
    stationResolver.getResolver(),
    sensorResolver.getResolver(),
    measureResolver.getResolver(),
];
