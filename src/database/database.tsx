let sqlite3 = require('sqlite3').verbose();
let md5 = require('md5');

const DBSOURCE = '/usr/src/app/db/db.sqlite';

let db = new sqlite3.Database(DBSOURCE, (err: any) => {
    if (err) {
      // Cannot open database
      console.error(err.message);
      throw err;
    } else {
        db.run(`CREATE TABLE user (
            id text PRIMARY KEY,
            firstname text,
            lastname text,
            email text UNIQUE,
            language text,
            readingFrequency integer,
            thresholdNotification integer,
            CONSTRAINT email_unique UNIQUE (email)
            )`,
        (err: any) => {
            if (err) {
                // Table already created
            } else {
                // Table just created, creating some rows
                const insert = 'INSERT INTO user (id, firstname, lastname, email, language, readingFrequency, thresholdNotification) VALUES (?,?,?,?,?,?,?)';
                db.run(insert, ['admin', 'admine', 'admin@example.com', 'fr', 500000, 20]);
                db.run(insert, ['user', 'usere', 'user@example.com', 'en', 500000, 20]);
            }
        });
        db.run(`CREATE TABLE stationSubscription (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            userID interger NOT NULL REFERENCES user(id),
            stationID integer NOT NULL
            )`,
        (err: any) => {
            if (err) {
                // Table already created
            } else {
                // Table just created, creating some rows
                const insert = 'INSERT INTO stationSubscription (userID, stationID) VALUES (?,?)';
                db.run(insert, ['1', '1']);
                db.run(insert, ['2', '1']);
            }
        });

    }
});

module.exports = db;