import * as express from 'express';
import {createServer} from 'http';
import * as moment from 'moment';
import * as React from 'react';
import * as ReactDOMServer from 'react-dom/server';
import { GraphQLSchema } from 'graphql';
import {makeExecutableSchema} from 'graphql-tools';
import * as rateLimit from 'express-rate-limit';

import * as dotenv from 'dotenv';

import * as graphqlHTTP from 'express-graphql';
import { resolvers } from './server/resolvers';
import { schemas } from './server/schema';
const db = require('./database/database');
// const md5 = require('md5');

import { AqiFetcher } from './server/aqiFetcher';
import * as fs from 'fs';
import axios from 'axios';
import bodyParser = require('body-parser');

moment.locale('fr');
dotenv.config();

const DEFAULT_THRESHOLD_VALUE = 20; // 20 PM default value for notifications
const DEFAULT_READING_FREQUENCY_VALUE = 500000; // Reading frequency every 5 minutes
const throttlingTimeGap = 60 * 1000; // 1 minute
const maxCall = 100; // 100 max calls per minute

try {
    const app = express();
    let auth0_management_token = '';

    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());

    if (process.env.PRODUCTION == 'true') {
        const options = {
            cert: fs.readFileSync(String(process.env.CERT_PATH)),
            key: fs.readFileSync(String(process.env.PRIVKEY_PATH)),
        };
        const https = require('https');
        https.createServer(options, app).listen(443);

        // Redirection http -> https (unable to make writeHead work for redirection)
        /*createServer(function (req, res) {
            res.writeHead(302, { 'Location' : process.env.APP_URL } );
            res.end();
        }).listen(process.env.PORT || 80);*/
    } else {
        createServer(app).listen(process.env.PORT || 80);
    }

    app.use(new rateLimit({
        windowMs: throttlingTimeGap,
        max: maxCall,
    }));

    const schema: GraphQLSchema = makeExecutableSchema({
        typeDefs: schemas,
        resolvers: resolvers,
    });

    app.use(bodyParser.urlencoded({extended: true}));
    app.use(bodyParser.json());

    app.use('/graphql',
            graphqlHTTP(req => ({
                schema,
                graphiql: true,
            })));

    app.use(express.static('dist/public'));

    // Get specific favorite Station from user
    app.get('/api/users/:id/favorites/:stationId', (req, res, next) => {
        const sql = 'SELECT * FROM stationSubscription WHERE userId = ? AND stationID = ?';
        const params = [req.params.id, req.params.stationId];
        db.get(sql, params, (err: any, row: any) => {
            if (err) {
              res.status(400).json({'error': err.message});
              return;
            }
            res.json({
                'message': 'success',
                'data': row,
            });
          });
    });

    // Get all favorites
    app.get('/api/users/favorites', (req, res, next) => {
        const sql = 'select * from stationSubscription';
        const params:  any = [];
        db.all(sql, params, (err: any, rows: any) => {
            if (err) {
              res.status(400).json({'error': err.message});
              return;
            }
            res.json({
                'message': 'success',
                'data': rows,
            });
        });
    });

    // Post a favorite to users
    app.post('/api/users/:id/favorites', (req, res, next) => {
        const errors: any = [];
        if (!req.body.stationId) {
            errors.push('No station specified');
        }
        const data = {
            stationId: req.body.stationId,
            userId: req.body.userId,
        };
        const sql = 'INSERT INTO stationSubscription (stationID, userID) VALUES (?,?)';
        const params = [data.stationId, data.userId];
        db.run(sql, params, function (err: any, result: any) {
            if (err) {
                res.status(400).json({'error': err.message});
                return;
            }
            res.json({
                'message': 'success',
                'data': data,
            });
        });
    });

    // Delete a specific favorite station from Revolvair
    app.delete('/api/users/:id/favorites/:stationId', (req, res, next) => {
        db.run(
            'DELETE FROM stationSubscription WHERE userID = ? and stationID = ?',
            req.params.id, req.params.stationId,
            function (err: any, result: any) {
                if (err) {
                    res.status(400).json({'error': result.message});
                    return;
                }
                res.json({'message': 'deleted', changes: this.changes});
        });
    });

    // GET favorites from a specific user
    app.get('/api/users/:id/favorites', (req, res, next) => {
        const sql = 'select * from stationSubscription where userId = ?';
        const params = [req.params.id];
        db.all(sql, params, (err: any, rows: any) => {
            if (err) {
              res.status(400).json({'error': err.message});
              return;
            }
            res.json({
                'message': 'success',
                'data': rows,
            });
        });
    });

    // SQLite Part for REST API Storage
    // Get a List of users from Revovair
    app.get('/api/users/', (req, res, next) => {
        const sql = 'select * from user';
        const params:  any = [];
        db.all(sql, params, (err: any, rows: any) => {
            if (err) {
              res.status(400).json({'error': err.message});
              return;
            }
            res.json({
                'message': 'success',
                'data': rows,
            });
        });
    });

    // Get a specific user from Revovair
    app.get('/api/users/:id/', (req, res, next) => {
        const sql = 'select * from user where id = ?';
        const params = [req.params.id];
        db.get(sql, params, (err: any, row: any) => {
            if (err) {
              res.status(400).json({'error': err.message});
              return;
            }
            res.json({
                'message': 'success',
                'data': row,
            });
          });
    });

    // Post new user for Revolvair
    app.post('/api/users/', (req, res, next) => {
        const jsontoken = require('jsonwebtoken');
        const userTokenDecoded = jsontoken.decode(req.body.__raw);
        const errors: any = [];
        if (!userTokenDecoded.email) {
            errors.push('No email specified');
        }
        if (errors.length) {
            res.status(400).json({'error': errors.join(',')});
            return;
        }
        const data = {
            id: userTokenDecoded.sub,
            firstname: userTokenDecoded.given_name,
            lastname: userTokenDecoded.fanullmily_name,
            email: userTokenDecoded.email,
            language: userTokenDecoded.locale,
            readingFrequency: DEFAULT_READING_FREQUENCY_VALUE,
            thresholdNotification: DEFAULT_THRESHOLD_VALUE,
        };
        const sql = 'INSERT INTO user (id, firstname, lastname, email, language, readingFrequency, thresholdNotification) VALUES (?,?,?,?,?,?,?)';
        const params = [data.id, data.firstname, data.lastname, data.email, data.language, data.readingFrequency, data.thresholdNotification];
        db.run(sql, params, function (err: any, result: any) {
            res.json({
                'message': 'success',
                'data': data,
                'id' : this.lastID,
            });
        });
    });
    // Update a specific user from Revolvair
    app.patch('/api/users/:id/', (req, res, next) => {
        const data = {
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            email: req.body.email,
            language: req.body.language,
            readingFrequency: req.body.readingFrequency,
            thresholdNotification: req.body.thresholdNotification,
        };
        db.run(
            `UPDATE user set
               firstname = COALESCE(?,firstname),
               lastname = COALESCE(?,lastname),
               email = COALESCE(?,email),
               language = COALESCE(?,language),
               readingFrequency = COALESCE(?,readingFrequency),
               thresholdNotification = COALESCE(?,thresholdNotification)
               WHERE id = ?`,
               [data.firstname, data.lastname, data.email, data.language, data.readingFrequency, data.thresholdNotification, req.params.id],
            function (err: any, result: any) {
                if (err) {
                    res.status(400).json({'error': result.message});
                    return;
                }
                res.json({
                    message: 'success',
                    data: data,
                    changes: this.changes,
                });
        });
    });

    // Delete a specific user from Revovair
    app.delete('/api/users/:id/', (req, res, next) => {
        db.run(
            'DELETE FROM user WHERE id = ?',
            req.params.id,
            function (err: any, result: any) {
                if (err) {
                    res.status(400).json({'error': result.message});
                    return;
                }
                res.json({'message': 'deleted', changes: this.changes});
        });
    });

    app.post('/api/user/profile/frequency', async (req, res, next) => {
        const newMeasure = req.body[1];

        const urlToPatch = 'https://' + process.env.AUTH0_DOMAIN + '/api/v2/users/' + req.body[0].loggedUserId;

        if (auth0_management_token == '') {
            auth0_management_token = await generateNewToken();
        }

        axios({
            method: 'patch',
            url: urlToPatch,
            headers: {
                'Authorization' : `Bearer ${auth0_management_token}`,
                'content-type' : 'application/json',
            },
            data: {
                'user_metadata': {'measureFrequency' : newMeasure},
            },
        }).then((response) => {
            // console.log(response);
        }, async (error) => {
            console.log('Error while accessing auth0, generating new token...');
            auth0_management_token = await generateNewToken();
        });
    });

    app.post('/api/user/profile/threshold', async (req, res, next) => {
        const newMeasure = req.body[1];

        const urlToPatch = 'https://' + process.env.AUTH0_DOMAIN + '/api/v2/users/' + req.body[0].loggedUserId;

        if (auth0_management_token == '') {
            auth0_management_token = await generateNewToken();
        }

        axios({
            method: 'patch',
            url: urlToPatch,
            headers: {
                'Authorization' : `Bearer ${auth0_management_token}`,
                'content-type' : 'application/json',
            },
            data: {
                'user_metadata': {'thresholdMeasure' : newMeasure},
            },
        }).then((response) => {
            // console.log(response);
        }, async (error) => {
            auth0_management_token = await generateNewToken();
        });
    });

    const aqiFetcher = new AqiFetcher();
    aqiFetcher.requestAqi();

    app.get('/*', (req, res, next) => {
        res.send(ReactDOMServer.renderToString(
            <html>
                <head>
                    <title>RevolvAir : Technologies pour un air pur</title>
                    <meta charSet='utf8'/>
                    <meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' />
                    <link rel='stylesheet' href='//cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css'/>
                    <link rel='stylesheet' href='//cdn.jsdelivr.net/chartist.js/latest/chartist.min.css'/>
                    <link rel='stylesheet' href='/css/styles.bundle.css'/>
                    <script src='//cdn.jsdelivr.net/chartist.js/latest/chartist.min.js'></script>
                </head>
                <body>
                    <main id='reactroot' className='revolvair'/>
                </body>
                <script src='/js/frontend.bundle.js'></script>
            </html>,
        ));
    });
} catch (error) {
    console.log(error);
    process.exit();
}

async function generateNewToken() {
    const client_id = process.env.AUTH0_SERVER_CLIENT_ID;
    const client_secret = process.env.AUTH0_SERVER_CLIENT_SECRET;
    const audience = 'https://' + process.env.AUTH0_DOMAIN + '/api/v2/';

    const urlToPost = 'https://' + process.env.AUTH0_DOMAIN + '/oauth/token';

    const params = {
        'grant_type': 'client_credentials',
        'client_id': client_id,
        'client_secret': client_secret,
        'audience': audience,
    };

    const data = Object.entries(params)
        .map(([key, val]) => `${key}=${encodeURIComponent(val)}`)
        .join('&');

    const options = {
        method: 'POST',
        headers : {
            'content-type': 'application/x-www-form-urlencoded',
        },
        data,
        url: urlToPost,
    };

    const response = await axios(options);
    console.log(response);
    return response.data.access_token;
}
