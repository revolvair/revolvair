import {Reducer} from 'redux';
import { AdminPageAllAction, AdminPageActions } from './AdminPageActions';

export interface AdminPageState {
    token: string;
}

export const defaultAdminPageState: AdminPageState = {
    token: '',
};

export const AdminPageReducer: Reducer<AdminPageState, AdminPageAllAction> = (state = defaultAdminPageState, action) => {
    switch (action.type) {
        case AdminPageActions.GENERATE_TOKEN:
            return {...state, token: action.token};
        default:
            return state;
    }
};
