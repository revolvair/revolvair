import * as React from 'react';
import TextField from '@material-ui/core/TextField/TextField';
import Button from '@material-ui/core/Button/Button';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';

export class EmailSubscriptionPage extends React.Component {
    public render(): React.ReactNode {
        return (
            <div className='email-subscription-page flex-align-center'>
                <Typography variant='h4' color='secondary' className='pb2'>S'inscrire à l'info-lettre de RevolvAir</Typography>
                <Paper className='p3'>
                    <form action='https://revolvair.us20.list-manage.com/subscribe/post?u=49dee2c0fb40dd2a1c7de02b1&amp;id=dc0f45f4d0' method='post' id='mc-embedded-subscribe-form' name='mc-embedded-subscribe-form' className='validate' target='_blank'>
                        <div className='flex-col'>
                            <div className='pb2'>
                                <TextField
                                    required
                                    type='email'
                                    label='Adresse courriel'
                                    placeholder='exemple@gmail.com'
                                    name='EMAIL'
                                />
                            </div>
                            <div className='pb2'>
                                <TextField
                                    label='Prénom'
                                    placeholder='Votre prénom'
                                    name='FNAME'
                                />
                            </div>
                            <div className='pb2'>
                                <TextField
                                    label='Nom de famille'
                                    placeholder='Votre nom de famille'
                                    name='LNAME'
                                />
                            </div>
                            <div className='pt2'>
                                <Button variant='contained' type='submit' name='subscribe'>
                                    S'inscrire
                                </Button>
                            </div>
                        </div>
                    </form>
                </Paper>
            </div>
        );
    }
}
