import {Divider} from '@material-ui/core';
import Avatar from '@material-ui/core/Avatar/Avatar';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography/Typography';
import * as React from 'react';
import {RouteComponentProps, StaticContext, withRouter} from 'react-router';
import {Dashboard} from '../applications/Dashboard';

export interface AboutUsProps extends RouteComponentProps<any, StaticContext, any> {
}

export class AboutUsPage extends React.Component<AboutUsProps> {
    public render(): React.ReactNode {
        return (
            <div>
                <Grid container spacing={40} style={{margin: 0, width: '100%'}}>
                    <Grid item xs={12}>
                        <Typography variant='h3' align='center' color='secondary'>À propos de RevolvAir</Typography>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <Typography variant='h4' color='secondary'>Les problématiques</Typography>
                        <Typography variant='body1' color='secondary' align='justify' className='pt2'>
                            À travers le monde, c'est 7 millions de décès prématurés annuels qui sont liés à la
                            pollution de l'air. À Québec, c'est 300 personnes qui perdront la vie à cause de la
                            combustion de bois et de carburants fossiles. À Québec, seulement 3 endroits sont échantillonnés.
                            La qualité de l'air peut varier considérablement d'un endroit à un autre. Il est donc primordial
                            de mettre en place une meilleure couverture de l'analyse de la qualité de l'air.
                        </Typography>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <Typography variant='h4' color='secondary'>Mission RevolvAir</Typography>
                        <Typography variant='body1' color='secondary' align='justify' className='pt2'>
                            L'objectif du projet RevolvAir est d'offrir aux citoyens des outils bon marché, qui leur permettront
                            de mieux comprendre la qualité de l'air intérieur et extérieur. La plate-forme nuagique RevolvAir permet
                            de rassembler les données de qualité de l'air pour une ville entière. Nous comptons sur la
                            participation des citoyens afin de mettre en place des stations d'analyse de la qualité de l'air bon marché.
                        </Typography>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <Typography variant='h4' color='secondary' className='pb2'>Activités polluantes</Typography>
                        <ul className='pl6' style={{listStyleType: 'square'}}>
                            <li><Typography variant='body1' color='secondary'>Combustion - bois de chauffage</Typography></li>
                            <li><Typography variant='body1' color='secondary'>Port de Québec</Typography></li>
                            <li><Typography variant='body1' color='secondary'>Usine de peinture Anacolor</Typography></li>
                            <li><Typography variant='body1' color='secondary'>Voitures sur les autoroutes</Typography></li>
                            <li><Typography variant='body1' color='secondary'>Incinérateurs</Typography></li>
                        </ul>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <img src='/images/industry.jpg' alt='industri' width='100%'/>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography variant='h3' align='center' color='secondary'>À propos des fines particules</Typography>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <Typography variant='h4' color='secondary'>Le PM2.5 et le PM10</Typography>
                        <Typography variant='body1' color='secondary' align='justify' className='pt2'>
                            Les fines particules passent par vos poumons et s'infiltrent dans votre système sanguin.
                            Ces particules causent des maladies cardiovasculaires, des cancers et affectent
                            vos yeux et poumons.
                        </Typography>
                        <img src='/images/particules.jpg' alt='particules' width='50%' className='p2'/>
                        <Typography variant='body1' color='secondary'><a href='https://fr.wikipedia.org/wiki/Particules_en_suspension'>Référence</a></Typography>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <Typography variant='h4' color='secondary'>Les normes</Typography>
                        <Typography  variant='body1' color='secondary' className='pt2'>OMS - (Organisation mondiale de la santé)</Typography>
                        <ul className='pl6' style={{listStyleType: 'square'}}>
                            <li><Typography variant='body1' color='secondary'>8 μg/m3 – moyenne annuelle</Typography></li>
                            <li><Typography variant='body1' color='secondary'>25 μg/m3 – moyenne sur 24h</Typography></li>
                        </ul>
                        <Typography variant='body1' color='secondary' className='pt2'>Canada</Typography>
                        <ul className='pl6' style={{listStyleType: 'square'}}>
                            <li><Typography variant='body1' color='secondary'>10 μg/m3 – moyenne annuelle</Typography></li>
                            <li><Typography variant='body1' color='secondary'>28 μg/m3 – moyenne sur 24h</Typography></li>
                        </ul>
                        <Typography variant='body1' color='secondary' className='pt2'>
                            Pour chaque tranche de 10 μg/m3 de PM 10, le risque de cancer du poumon augmente de 22 %.
                        </Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography variant='h3' align='center' color='secondary'>Nous contacter</Typography>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <Typography variant='h4' color='secondary' className='pb2'>L'équipe RevolvAir</Typography>
                            <List style={{width: '100%', maxWidth: '360', backgroundColor: 'white'}}>
                                <ListItem>
                                    <ListItemAvatar>
                                        <Avatar src='/images/anthoPicture.jpg' />
                                    </ListItemAvatar>
                                    <ListItemText
                                        primary= {
                                            <Typography style={{fontWeight: 'bold'}}>Anthony Collin-Nadeau</Typography>
                                        }
                                        secondary={
                                            <React.Fragment>
                                                <Typography>The best programmer in the Universe</Typography>
                                                <Typography><a href='http://anthonycollinnadeau.com/'>Site web</a></Typography>
                                            </React.Fragment>
                                        }
                                    />
                                </ListItem>
                                <Divider/>
                                <ListItem>
                                    <ListItemAvatar>
                                        <Avatar src='/images/marcoPicture.jpg' />
                                    </ListItemAvatar>
                                    <ListItemText
                                        primary= {
                                            <Typography style={{fontWeight: 'bold'}}>Marc-Olivier Bouchard</Typography>
                                        }
                                        secondary={
                                            <React.Fragment>
                                                <Typography><a href='https://github.com/marcolivierbouch'>GitHub</a></Typography>
                                            </React.Fragment>
                                        }
                                    />
                                </ListItem>
                                <Divider/>
                                <ListItem>
                                    <ListItemAvatar>
                                        <Avatar src='/images/vgPicture.jpg' />
                                    </ListItemAvatar>
                                    <ListItemText
                                        primary= {
                                            <Typography style={{fontWeight: 'bold'}}>Vincent-Gabriel Proulx</Typography>
                                        }
                                        secondary={
                                            <React.Fragment>
                                                <Typography>ThinkPad master</Typography>
                                                <Typography><a href='https://gitlab.com/netvincent'>GitLab</a></Typography>
                                            </React.Fragment>
                                        }
                                    />
                                </ListItem>
                                <Divider/>
                                <ListItem>
                                    <ListItemAvatar>
                                        <Avatar src='/images/jbPicture.jpg' />
                                    </ListItemAvatar>
                                    <ListItemText
                                        primary= {
                                            <Typography style={{fontWeight: 'bold'}}>Jean-Benoit Rossignol</Typography>
                                        }
                                        secondary={
                                            <React.Fragment>
                                                <Typography>GoogleSheet script master</Typography>
                                                <Typography>
                                                    <a href='https://www.facebook.com/jeanbenoit.rossignol.5'>Facebook </a>
                                                    <a href='https://gitlab.com/users/Jean-Benoit1'>GitLab</a>
                                                </Typography>
                                            </React.Fragment>
                                        }
                                    />
                                </ListItem>
                        </List>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <Typography variant='h4' color='secondary'>Inscrivez-vous à notre info-lettre</Typography>
                        <ul className='text pl6' style={{listStyleType: 'square'}}>
                            <li><Typography variant='body1' color='secondary'><a href={Dashboard.SUBSCRIPTION_PAGE}>Notre info-lettre</a></Typography></li>
                        </ul>
                        <Typography variant='h4' color='secondary'>Suivez-nous</Typography>
                        <ul className='text pl6' style={{listStyleType: 'square'}}>
                            <li><Typography variant='body1' color='secondary'><a href='https://www.facebook.com/RevolvAir-2206425182723346'>Facebook</a></Typography></li>
                        </ul>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

export const AboutUsPageWithRouter = withRouter(AboutUsPage);
