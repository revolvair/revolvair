import {Action} from 'redux';
import {Station} from '../../models/Station';

export enum StationPageActions {
    GET_STATION = 'GET_STATION',
}

export type StationPageAction = Action<StationPageActions>;
export type StationPageAllAction = Partial<StationPageGetStation> & StationPageAction;
export type StationPageGetStation = {type: StationPageActions} & {station: Station};

export function getStation(station: Station): StationPageGetStation {
    return {
        type: StationPageActions.GET_STATION,
        station,
    };
}
