import {defaultAdminPageState, AdminPageState, AdminPageReducer} from './AdminPageReducer';
import {updateToken} from './AdminPageActions';

describe('AdminPageReducer', () => {
    it('Default token state should be an empty str by default', () => {
        expect(defaultAdminPageState.token).toEqual('');
    });

    it('Should return default state if no action corresponding', () => {
        const state: AdminPageState = AdminPageReducer(undefined, {type: undefined});
        expect(state.token).toEqual(defaultAdminPageState.token);
    });

    it('Should return default state if action corresponding', () => {
        const token = 'token';
        const state: AdminPageState = AdminPageReducer(undefined, updateToken(token));
        expect(state.token).toEqual(token);
    });
});
