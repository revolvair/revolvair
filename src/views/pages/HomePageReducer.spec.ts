import {defaultHomePageState, HomePageReducer, HomePageState} from './HomePageReducer';
import {updateLocation, updateOrder} from './HomePageActions';
import {updateZoom} from './HomePageActions';
import {Point} from '../components/GoogleMap';
import {orderDirection, TableComponent} from '../components/TableComponent';

describe('HomePageReducer', () => {
    it('DefaultHomePageState should contain the quebec location by default', () => {
        expect(defaultHomePageState.location).toEqual({lat: 46.8138, lng: -71.2079});
    });

    it('Should return default state if no action corresponding', () => {
        const state: HomePageState = HomePageReducer(undefined, {type: undefined});
        expect(state.location).toEqual(defaultHomePageState.location);
    });

    it('Should return default state if action corresponding', () => {
        const location: Point = {lat: 70, lng: -69};
        const state: HomePageState = HomePageReducer(undefined, updateLocation(location));
        expect(state.location).toEqual(location);
    });

    it('Should return new zoom if action corresponding', () => {
        const zoom: number = 10;
        const state: HomePageState = HomePageReducer(undefined, updateZoom(zoom));
        expect(state.zoom).toEqual(zoom);
    });

    it('Should return new order and orderRow if action corresponding', () => {
        const order: orderDirection = TableComponent.DESCENDANT_SORT;
        const orderRow: number = 1;
        const state: HomePageState = HomePageReducer(undefined, updateOrder(order, orderRow));
        expect(state.order).toEqual(order);
        expect(state.orderRow).toEqual(orderRow);
    });
});
