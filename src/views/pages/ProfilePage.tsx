import * as React from 'react';
import { withRouter } from 'react-router';
import { Grid, Card, Typography, CardContent, Avatar} from '@material-ui/core';
import { Theme } from '@material-ui/core/styles';
import { createStyles, makeStyles } from '@material-ui/styles';
import { MeasureFrequency } from '../components/MeasureFrequency';
import { NotificationThreshold } from '../components/NotificationThreshold';
import NotificationsIcon from '@material-ui/icons/Notifications';
import EqualizerIcon from '@material-ui/icons/Equalizer';
import {useAuth0} from '../../utils/hook';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: 10,
      height: 20,
    },
    large: {
      width: '20%',
      height: '20%',
    },
  }),
);

const ProfilePage = () => {
    const { loading, user } = useAuth0();
    const ICON_WHITE_COLOR = 'secondary';
    const classes = useStyles();

    if (loading || !user) {
      return <div>Vous n'êtes pas connecté!</div>;
    }

    return (
      <React.Fragment>
          <div className='profile-page'>
                    <Grid container direction='column' justify='flex-start' alignItems='center'>
                        <Avatar alt='Profile' src={user.picture} className={classes.large}/>
                        <br></br>
                        <h1>{user.name}</h1>
                    </Grid>
                    <br></br>
                    <Grid  container direction='row' justify='space-evenly' alignItems='center'>
                        <Card style={{display: 'flex'}}>
                            <div>
                                <CardContent style={{flex: '1 0 auto', color: 'white'}}>
                                    <Typography component='h5' variant='h5' style={{color: 'white'}}>
                                        Mesure
                                    </Typography>
                                    <br></br>
                                    <MeasureFrequency loggedUserId={user.sub}/>
                                </CardContent>
                            </div>
                            <EqualizerIcon color={ICON_WHITE_COLOR} style={{ fontSize: 150 }}/>
                        </Card>
                        <Card style={{display: 'flex'}}>
                            <div>
                                <CardContent style={{flex: '1 0 auto'}}>
                                    <Typography component='h5' variant='h5' style={{color: 'white'}}>
                                        Notification
                                    </Typography>
                                    <br></br>
                                    <NotificationThreshold loggedUserId={user.sub}/>
                                </CardContent>
                            </div>
                            <NotificationsIcon color={ICON_WHITE_COLOR} style={{ fontSize: 150 }}/>
                        </Card>
                    </Grid>
                </div>
      </React.Fragment>
    );
};

export const ProfilePageWithRouter = withRouter(ProfilePage);
