import {Point} from '../components/GoogleMap';
import {HomePageActions, updateLocation, updateZoom, updateOrder} from './HomePageActions';
import {orderDirection, TableComponent} from '../components/TableComponent';

describe('HomePageActions', () => {
    it('UPDATE_MAP_LOCATION should equal UPDATE_MAP_LOCATION', () => {
        expect(HomePageActions.UPDATE_MAP_LOCATION).toBe('UPDATE_MAP_LOCATION');
    });

    it('UPDATE_MAP_ZOOM should equal UPDATE_MAP_ZOOM', () => {
        expect(HomePageActions.UPDATE_MAP_ZOOM).toBe('UPDATE_MAP_ZOOM');
    });

    it('UPDATE_ORDER_TABLE should equal UPDATE_ORDER_TABLE', () => {
        expect(HomePageActions.UPDATE_ORDER_TABLE).toBe('UPDATE_ORDER_TABLE');
    });

    describe('updateLocation', () => {
        it('Should return an action with the location pass as params', () => {
            const location: Point = {lat: 4, lng: 9};
            const action = updateLocation(location);
            expect(action.type).toBe(HomePageActions.UPDATE_MAP_LOCATION);
            expect(action.location).toEqual(location);
        });
    });

    describe('updateZoom', () => {
        it('Should return an action with the zoom pass as params', () => {
            const zoom: number = 1;
            const action = updateZoom(zoom);
            expect(action.type).toBe(HomePageActions.UPDATE_MAP_ZOOM);
            expect(action.zoom).toEqual(zoom);
        });
    });

    describe('updateOrder', () => {
        it('Should return an action with the order and orderRow pass as params', () => {
            const order: orderDirection = TableComponent.ASCENDANT_SORT;
            const orderRow: number = 0;
            const action = updateOrder(order, orderRow);
            expect(action.type).toBe(HomePageActions.UPDATE_ORDER_TABLE);
            expect(action.order).toEqual(order);
            expect(action.orderRow).toEqual(orderRow);
        });
    });
});
