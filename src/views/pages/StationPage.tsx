import * as React from 'react';
import {useEffect} from 'react';
import {connect} from 'react-redux';
import {RevolvairState} from '../AppReducer';
import ApolloClient from 'apollo-client';
import {NormalizedCacheObject} from 'apollo-cache-inmemory';
import {Station} from '../../models/Station';
import {Dispatch} from 'redux';
import {getStation} from './StationPageActions';
import gql from 'graphql-tag';
import Grid from '@material-ui/core/Grid/Grid';
import {HomePage} from './HomePage';
import {Formatter} from '../../utils/Formatter';
import Badge from '@material-ui/core/Badge';
import {Utils} from '../../utils/Utils';
import {SlideMenu} from '../components/SlideMenu';
import {Graph, GraphData, ImportantValues} from '../components/Graph';
import {ImportantValue} from '../components/ImportantValue';
import * as moment from 'moment';
import CircularProgress from '@material-ui/core/CircularProgress/CircularProgress';

import IconButton from '@material-ui/core/IconButton';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import LastPageIcon from '@material-ui/icons/LastPage';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableCell from '@material-ui/core/TableCell';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableBody from '@material-ui/core/TableBody';
import {isDefined, isNotEmpty } from 'welshguard';
// import {useAuth0} from '../../utils/hook';
import {FacebookComment} from '../components/FacebookComment';
import {useAuth0} from '../../utils/hook';
import {Sensor} from '../../models/Sensor';
import {AqiCalculator} from '../../utils/AqiCalculator';

interface StationGraphData {
    data: GraphData[];
    importantValues: ImportantValues;
}

const mapStateToProps = ({stationPage}: RevolvairState): StationPageStateToProps => ({
    station: stationPage.station,
});

export interface StationPageProps extends StationPageStateToProps, StationPageDispatchToProps {
    stationId: number;
    apolloClient: ApolloClient<NormalizedCacheObject>;
}

interface StationPageStateToProps {
    station: Station;
}

interface StationPageDispatchToProps {
    getStation: (station: Station) => void;
}

const mapDispatchToProps = (dispatch: Dispatch): StationPageDispatchToProps => ({
    getStation: (station: Station) => dispatch(getStation(station)),
});

const FACEBOOK_APP_ID = 'RevolvAir-2206425182723346';

const GET_STATION = gql`
query station($id: Int!) {
    station(id: $id) {
        name
        sensors {
            measures(timeRange: "now-30d") {
                value
                timestamp
            }
        }
        notifications {
            oldValue
            newValue
            timestamp
            oldState
            newState
        }
    }
}`;

interface TablePaginationActionsProps {
    count: number;
    page: number;
    rowsPerPage: number;
    onChangePage: (event: React.MouseEvent<HTMLButtonElement>, newPage: number) => void;
}

// const { loading, user } = useAuth0();

function TablePaginationActions(props: TablePaginationActionsProps) {
    const { count, page, rowsPerPage, onChangePage } = props;

    const handleFirstPageButtonClick = (event: React.MouseEvent<HTMLButtonElement>) => {
      onChangePage(event, 0);
    };

    const handleBackButtonClick = (event: React.MouseEvent<HTMLButtonElement>) => {
      onChangePage(event, page - 1);
    };

    const handleNextButtonClick = (event: React.MouseEvent<HTMLButtonElement>) => {
      onChangePage(event, page + 1);
    };

    const handleLastPageButtonClick = (event: React.MouseEvent<HTMLButtonElement>) => {
      onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
    };

    return (
      <div className='arrows'>
        <IconButton
            color='secondary'
            onClick={handleFirstPageButtonClick}
            disabled={page === 0}
            aria-label='first page'
        >
            <FirstPageIcon />
        </IconButton>
        <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label='previous page' color='secondary'>
            &lt;
        </IconButton>
        <IconButton
            color='secondary'
            onClick={handleNextButtonClick}
            disabled={page >= Math.ceil(count / rowsPerPage) - 1}
            aria-label='next page'
            >
            &gt;
            </IconButton>
            <IconButton
                color='secondary'
                onClick={handleLastPageButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                aria-label='last page'
                >
            <LastPageIcon />
        </IconButton>
      </div>
    );
}

const AqiValue = ( value: any ) => {
    const notificationsArray = value.value as Sensor[];
    
    const selectedSensor = notificationsArray[notificationsArray.length - 1];

    const measuresList = selectedSensor.measures;
    const lastMeasure = measuresList[measuresList.length - 1];

    const aqiCalculator = new AqiCalculator();
    let aqiValue = aqiCalculator.ConcentrationPM25(lastMeasure.value);

    aqiValue = +((Math.round(aqiValue * 100) / 100).toFixed(2));
    return (
        <div>
            <h2>Valeur de l'aqi pour la station : {aqiValue}</h2>
        </div>
    );
};

export function StationPage(Props: StationPageProps) {
    const DATE_FORMAT_NOTIFICATION = 'Do MMMM YYYY, H:mm ';

    // At each interval, loads the data from station
    useEffect(() => {
        loadData(Props);
        // let intervalTime = 20000;
        // if (isDefined(user.user_metadata.measureFrequency)) {
        //     intervalTime = user.user_metadata.measureFrequency;
        // }
        const interval = setInterval(() => {
            loadData(Props);
        }, 20000);
        return () => clearInterval(interval);
    }, []);

    let rows;
    // let emptyRows;

    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);

    if (isDefined(Props.station)) {
        rows = Props.station.notifications;
        // emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);
    }

    const handleChangePage = (event: React.MouseEvent<HTMLButtonElement> | null, newPage: number) => {
      setPage(newPage);
    };

    const handleChangeRowsPerPage = (
      event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    ) => {
      setRowsPerPage(parseInt(event.target.value, 10));
      setPage(0);
    };

    const { user } = useAuth0();

    return (
        <div className='station-page'>
            {isDefined(Props.station) ? (
                <div>
                    <Grid container justify='center' direction='column' spacing={0}>
                        {Props.station.sensors && isNotEmpty(Props.station.sensors)
                            ? getGraph(getDataList(Props), Props, user)
                            : (
                                <Grid item xs={11} sm={8}>
                                    <p>Il n'y a pas de capteurs sur cette station.</p>
                                </Grid>
                            )}
                    </Grid>
                {isDefined(Props.station.notifications) ? (
                <Grid>
                    <br />
                    <AqiValue value={Props.station.sensors} />
                    <br /> <br />
                    <h2 className='pb2'>Notifications</h2>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Date</TableCell>
                                <TableCell>Statut</TableCell>
                                <TableCell>Ancienne valeur</TableCell>
                                <TableCell>Nouvelle valeur</TableCell>
                            </TableRow>
                        </TableHead>
                            <TableBody>
                                {(rowsPerPage > 0
                                    ? rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                    : rows
                                ).map(row => (
                                <TableRow className = 'table-row' key = {row.stationId + row.timestamp}>
                                    <TableCell>
                                        {Formatter.formatDateNotification(DATE_FORMAT_NOTIFICATION, + row.timestamp)}
                                    </TableCell>
                                    <TableCell>
                                        {`La valeur est passée de ${row.oldState} à ${row.newState}`}
                                    </TableCell>
                                    <TableCell>
                                        <Badge classes={{
                                            badge: Utils.getColor(row.oldValue, {
                                                bad: HomePage.RED_COLOR_BACKGROUND,
                                                warning: HomePage.YELLOW_COLOR_BACKGROUND,
                                                good: HomePage.GREEN_COLOR_BACKGROUND,
                                        })}}>
                                            {row.oldValue}
                                        </Badge>
                                    </TableCell>
                                    <TableCell>
                                        <Badge classes={{
                                            badge: Utils.getColor(row.newValue, {
                                                bad: HomePage.RED_COLOR_BACKGROUND,
                                                warning: HomePage.YELLOW_COLOR_BACKGROUND,
                                                good: HomePage.GREEN_COLOR_BACKGROUND,
                                        })}}>
                                            {row.newValue}
                                        </Badge>
                                    </TableCell>
                                </TableRow>
                                ))}
                            </TableBody>
                            <TableFooter>
                                <TableRow>
                                    <TablePagination
                                        rowsPerPageOptions={[1, 10, 25, 50, 100]}
                                        count={rows.length}
                                        rowsPerPage={rowsPerPage}
                                        page={page}
                                        labelRowsPerPage={'Notifications par page'}
                                        SelectProps={{
                                            // inputProps: { 'aria-label': 'rows per page' },
                                            native: false,
                                        }}
                                        style={{color: 'white'}}
                                        onChangePage={handleChangePage}
                                        onChangeRowsPerPage={handleChangeRowsPerPage}
                                        ActionsComponent={TablePaginationActions}
                                    />
                                </TableRow>
                            </TableFooter>
                        </Table>
                        <h2>
                            Commentaires
                        </h2>
                    </Grid>
                    ) : null }
                </div>
            ) : ( <CircularProgress color='secondary'/> )}

            <FacebookComment appId={FACEBOOK_APP_ID} url= {window.location.href}/>
        </div>
    );
}

function loadData(props: StationPageProps) {
    props.apolloClient.cache.reset();
    props.apolloClient.query<{station: Station[]}>({query: GET_STATION, variables: {id: + props.stationId}})
        .then(({data}) => props.getStation(data.station[0]));
}

function getGraph(dataList: StationGraphData[], Props: StationPageProps, user: any): React.ReactNode {

    const DataKey: string = Formatter.DataKey;
    return (
        <SlideMenu
            stationId={Props.stationId}
            stationName={Props.station.name}
            user={user}
            listName={['Aujourd\'hui', 'Dernière semaine', 'Dernier mois', 'Dernière année']}
            menuData={dataList}
            tabContainer={(data) => (
                <div className='flex-align-center tabContainer pb2' key={JSON.stringify(data)}>
                    <div>
                        <Graph
                            dataDescription='Particules de combustion, composé organique, métaux, etc.'
                            width={1400}
                            title='PM2.5'
                            data={data.data}
                            dataKeyX='name'
                            dataKeyY={DataKey}
                            colorLine='#7EDCF7'
                        />
                    </div>
                    <div className='station-page-important-values flex-between'>
                        <ImportantValue id='maximum' name='Maximum' value={data.importantValues.max} className='mx1'/>
                        <ImportantValue id='minimum' name='Minimum' value={data.importantValues.min} className='mx1'/>
                        <ImportantValue id='average' name='Moyenne' value={data.importantValues.avg} className='mx1'/>
                    </div>
                </div>
            )}
        />
    );
}

function getDataList(Props: StationPageProps): StationGraphData[] {
    const now = moment().endOf('day');
    const allData: Formatter.Info[] = Formatter.formatMeasures(Props.station.sensors[0].measures);

    const days = Formatter.formatGraphData(Utils.TimeGap.Day, allData, 'H[h]', now, true);
    const weeks = Formatter.formatGraphData(Utils.TimeGap.Week, allData, 'M/D', now, true);
    const months = Formatter.formatGraphData(Utils.TimeGap.Month, allData, 'M/D', now, true);
    const years = Formatter.formatGraphData(Utils.TimeGap.Year, allData, 'M/Y', now, true);

    return [
        {
            data:        days,
            importantValues: Formatter.formatImportantValuesFromGraphData(days.map(day => day.tooltip)),
        },
        {
            data:        weeks,
            importantValues: Formatter.formatImportantValuesFromGraphData(weeks.map(week => week.tooltip)),
        },
        {
            data:        months,
            importantValues: Formatter.formatImportantValuesFromGraphData(months.map(month => month.tooltip)),
        },
        {
            data:   years,
            importantValues: Formatter.formatImportantValuesFromGraphData(years.map(year => year.tooltip)),
        },
    ];
}


export const StationPageConnected = connect(mapStateToProps, mapDispatchToProps)(StationPage);
