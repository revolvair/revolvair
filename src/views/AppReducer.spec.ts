import {revolvairReducer, defaultRevolvairState} from './AppReducer';
import {AnyAction} from 'redux';
describe('RevolvairReducer', () => {
    it('should return default state if it undefined', () => {
        const action: AnyAction = {type: undefined};
        expect(revolvairReducer(undefined, action)).toEqual(defaultRevolvairState);
    });
});