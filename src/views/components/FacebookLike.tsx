import * as React from 'react';
import { FacebookProvider, Like } from 'react-facebook';

export interface FacebookLikeContainerProps {
    className?: string;
    appId: string;
    url: string;
}

export class FacebookLike extends React.Component<FacebookLikeContainerProps> {
    public render(): React.ReactNode {
        return (
            <div className={this.props.className}>
                <FacebookProvider appId={this.props.appId} language='fr_FR'>
                    <Like
                        href={this.props.url}
                        layout='button_count'
                        colorScheme='dark'
                        showFaces
                        share
                    />
                </FacebookProvider>
            </div>
        );
    }
}
