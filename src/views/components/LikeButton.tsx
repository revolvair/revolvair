import * as React from 'react';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import Button from '@material-ui/core/Button';
import axios from 'axios';

export interface LikeButtonProps<> {
    stationId: number;
    user: any;
}

export function LikeButton (props: LikeButtonProps) {
        return (
            <Button onClick={() => { addToFavorites(props.stationId, props.user.sub); }}>
                    <FavoriteBorderIcon color='secondary'/>
            </Button>
        );
}

function addToFavorites(stationId: number, userId: any) {
    axios.post('/api/users/' + userId + '/favorites' , {'stationId' : stationId, 'userId' : userId});
}
