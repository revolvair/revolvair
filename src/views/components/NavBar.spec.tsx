import * as Enzyme from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';
import {NavBarWithRouter} from './NavBar';
import * as React from 'react';

describe('<NavBar/>', () => {
    beforeAll(() => Enzyme.configure({ adapter: new Adapter()}));
    it('should not throw error on build', () => {
        expect(() => Enzyme.shallow(<NavBarWithRouter/>)).not.toThrow();
    });
});
