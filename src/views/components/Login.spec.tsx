import * as Enzyme from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';
import {Login} from './Login';
import * as React from 'react';

describe('<Login/>', () => {
    beforeAll(() => Enzyme.configure({ adapter: new Adapter()}));
    it('should not throw error on build', () => {
        expect(() => Enzyme.shallow(<Login/>)).not.toThrow();
    });
});
