import * as React from 'react';
import Drawer from '@material-ui/core/Drawer/Drawer';
import {RevolvairState} from '../AppReducer';
import {DashboardAction, changeDrawer} from '../applications/DashboardActions';
import {Dispatch} from 'redux';
import {withRouter, RouteComponentProps} from 'react-router-dom';
import {StaticContext} from 'react-router';
import {connect} from 'react-redux';
import IconButton from '@material-ui/core/IconButton/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import HomeIcon from '@material-ui/icons/Home';
import AccountBoxIcon from '@material-ui/icons/AccountBox';
import SecurityIcon from '@material-ui/icons/Security';
import InfoIcon from '@material-ui/icons/InfoOutlined';
import MailIcon from '@material-ui/icons/Mail';
import LockIcon from '@material-ui/icons/Lock';
import List from '@material-ui/core/List/List';
import ListItem from '@material-ui/core/ListItem/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText/ListItemText';
import {Dashboard} from '../applications/Dashboard';
import { useAuth0 } from '../../utils/hook';

interface DrawerStateToProps {
    drawer: boolean;
}

const mapStateToProps = ({dashboard}: RevolvairState): DrawerStateToProps => ({
    drawer: dashboard.drawer,
});

interface DrawerDispatchToProps {
    changeDrawer: (drawer: boolean) => DashboardAction;
}

const mapDispatchToProps = (dispatch: Dispatch): DrawerDispatchToProps => ({
    changeDrawer: (drawer: boolean) => dispatch(changeDrawer(drawer)),
});

export interface SideMenuContainerProps extends RouteComponentProps<any, StaticContext, any>, DrawerDispatchToProps, DrawerStateToProps {
}

const ICON_WHITE_COLOR = 'secondary';

const SideMenu = (props: SideMenuContainerProps) => {
    const {isAuthenticated, loginWithRedirect} = useAuth0();

    const loginHandler = async (e: any) => {
        e.preventDefault();
        if (loginWithRedirect) {
            await loginWithRedirect();
        }
    };

    return (
        <div>
            <IconButton
                aria-label='Menu'
                onClick={() => {
                    props.changeDrawer(true);
            }}>
                <MenuIcon color='secondary'/>
            </IconButton>
            <Drawer
                open={props.drawer}
                onClose={() => {
                    props.changeDrawer(false);
            }}>
                <div
                    tabIndex={0}
                    role='button'
                    onClick={() => {
                        props.changeDrawer(false);
                }}>
                    <List>
                        <ListItem>
                            <img src='/images/logo.svg' className='image'/>
                            <ListItemText primary='Menu'/>
                        </ListItem>
                        <ListItem
                            button
                            onClick={() => props.history.location.pathname === Dashboard.HOME_PAGE ? '' : props.history.push(Dashboard.HOME_PAGE)
                        }>
                            <ListItemIcon>
                                <HomeIcon color={ICON_WHITE_COLOR}/>
                            </ListItemIcon>
                            <ListItemText primary='Accueil'/>
                        </ListItem>
                        {isAuthenticated &&
                            <ListItem
                                button
                                onClick={() =>
                                    props.history.location.pathname === Dashboard.PROFILE_PAGE ? '' : props.history.push(Dashboard.PROFILE_PAGE)
                                }>
                                <ListItemIcon>
                                    <AccountBoxIcon color={ICON_WHITE_COLOR}/>
                                </ListItemIcon>
                                <ListItemText primary='Profil'/>
                            </ListItem>
                        }
                        <ListItem
                            button
                            onClick={() => props.history.location.pathname === Dashboard.INFO_PAGE ? '' : props.history.push(Dashboard.INFO_PAGE)
                        }>
                            <ListItemIcon>
                                <InfoIcon color={ICON_WHITE_COLOR}/>
                            </ListItemIcon>
                            <ListItemText primary='À propos'/>
                        </ListItem>
                        <ListItem
                            button
                            onClick={() => props.history.location.pathname === Dashboard.ADMIN_PAGE ? '' : props.history.push(Dashboard.ADMIN_PAGE)
                        }>
                            <ListItemIcon>
                                <SecurityIcon color={ICON_WHITE_COLOR}/>
                            </ListItemIcon>
                            <ListItemText primary='Token'/>
                        </ListItem>
                        <ListItem
                            button
                            onClick={() =>
                                props.history.location.pathname === Dashboard.SUBSCRIPTION_PAGE ? '' : props.history.push(Dashboard.SUBSCRIPTION_PAGE)
                        }>
                            <ListItemIcon>
                                <MailIcon color={ICON_WHITE_COLOR}/>
                            </ListItemIcon>
                            <ListItemText primary='Info-lettre'/>
                        </ListItem>
                        {!isAuthenticated && (
                            <ListItem
                                button
                                onClick={loginHandler}>
                                <ListItemIcon>
                                    <LockIcon color={ICON_WHITE_COLOR}/>
                                </ListItemIcon>
                                <ListItemText primary='Connexion'/>
                            </ListItem>
                        )}
                    </List>
                </div>
            </Drawer>
        </div>
    );
};

export const SideMenuWithRouter = withRouter(SideMenu);
export const SideMenuConnected = connect(mapStateToProps, mapDispatchToProps)(SideMenuWithRouter);
