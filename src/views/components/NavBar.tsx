import Toolbar from '@material-ui/core/Toolbar/Toolbar';
import * as React from 'react';
import {withRouter, RouteComponentProps} from 'react-router-dom';
import {StaticContext} from 'react-router';
import {SideMenuConnected} from './SideMenu';
import {Dashboard} from '../applications/Dashboard';
import {Login} from './Login';

export interface NavBarProps extends RouteComponentProps<any, StaticContext, any> {
}

const NavBar = (props: NavBarProps) => {
    return (
        <nav className='nav-bar'>
            <Toolbar>
                <SideMenuConnected/>
                <div
                    className='nav-bar-title'
                    onClick={() => props.history.location.pathname === Dashboard.HOME_PAGE ? '' : props.history.push(Dashboard.HOME_PAGE)
                }>
                    <img src='/images/logo.svg' className='image' />
                </div>
                <div
                    className='nav-bar-title ml2'
                    onClick={() => props.history.location.pathname === Dashboard.HOME_PAGE ? '' : props.history.push(Dashboard.HOME_PAGE)
                }>
                    <div className='flex-align-center-title'>
                        <h1 className='title'>RevolvAir</h1>
                        <h2 className='description'>Technologies pour un air pur</h2>
                    </div>
                </div>
                <div className='login-button'>
                    <Login/>
                </div>
            </Toolbar>
        </nav>
    );
};

export const NavBarWithRouter = withRouter(NavBar);
