import * as React from 'react';
import AppBar from '@material-ui/core/AppBar/AppBar';
import Tabs from '@material-ui/core/Tabs/Tabs';
import Tab from '@material-ui/core/Tab/Tab';
import withTheme from '@material-ui/core/styles/withTheme';
import Grid from '@material-ui/core/Grid/Grid';
import { LikeButton } from './LikeButton';
import { UnlikeButton } from './UnlikeButton';
import {isDefined} from 'welshguard';
import axios from 'axios'; export

interface SliderMenuContainerProps<T> {
    user: any;
    stationId: number;
    stationName: string;
    listName: string[];
    menuData: T[];
    tabContainer: (props: T) => React.ReactNode;
}
export let button: any = null;
async function isStationLiked(stationId: number, userId: any) {
    const res = await axios.get('/api/users/' + userId + '/favorites/' + stationId);
    if (res.data.data == null) {
        button = <LikeButton user={userId} stationId={stationId}></LikeButton>;
    } else {
        button = <UnlikeButton user={userId} stationId={stationId}></UnlikeButton>;
    }
}
export class SlideMenu<T extends object> extends React.Component<SliderMenuContainerProps<T>> {
    state = {
        value: 0,
    };
    handleChange = (event: React.ChangeEvent, value: string) => {
        this.setState({ value });
    }
    public render(): React.ReactNode {
        const { value } = this.state;
        if (isDefined(this.props.user)) {
            isStationLiked(this.props.stationId, this.props.user.sub);
        }
        return (
            <div className='slide-menu'>
                <Grid item className='py2' xs={12} >
                    <AppBar position='static' className='appbar' >
                        <Grid justify='space-between' container spacing={0}>
                            <Grid item>
                                <h2 className='station-name'>{this.props.stationName}
                                    {this.props.user &&
                                        button
                                    }
                                </h2>
                            </Grid>
                            <Grid item>
                                <Tabs value={value} onChange={this.handleChange} className='tabs mb2' classes={{indicator: 'tabIndicator'}}>
                                    {this.props.listName.map((name, index) => <Tab key={name} label={name}
                                    className={this.state.value === index ? 'tabActive' : 'tabText'}/>)}
                                </Tabs>
                            </Grid>
                        </Grid>
                    </AppBar>
                    {this.props.menuData.map(
                        (menu, index) => value === index && this.props.tabContainer(menu),
                    )}
                </Grid>
            </div>
        );
    }
}

export const SlideMenuWithTheme = withTheme()(SlideMenu);

