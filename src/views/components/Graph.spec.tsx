import * as Enzyme from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';
import * as React from 'react';
import {Graph} from './Graph';

describe('<Graph/>', () => {
    beforeAll(() => Enzyme.configure({ adapter: new Adapter()}));

    it('should not throw error on build', () => {
        expect(() => Enzyme.shallow(<Graph dataKeyY='testValue' dataKeyX='' dataDescription='' data={[]} title='' colorLine='' width={0}/>)).not.toThrow();
    });
});
