import * as Enzyme from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';
import * as React from 'react';
import {ChartistPie} from './ChartistPie';

describe('<ChartistPie/>', () => {
    beforeAll(() => Enzyme.configure({ adapter: new Adapter()}));

    it('should not throw error on build', () => {
        expect(() => Enzyme.shallow(<ChartistPie id='testValue' totalGraph={200} values={[{value: 10, className: 'test'}]} />)).not.toThrow();
    });
});
