import * as React from 'react';
import { Theme } from '@material-ui/core/styles';
import { createStyles, makeStyles } from '@material-ui/styles';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import axios from 'axios';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      minWidth: 420,
      color: 'white',
    },
    MenuItem: {
      minWidth: 420,
      color: 'white',
    },
  }),
);

let threshold = 20; // Default value of threshold measure

export const NotificationThreshold = (loggedUserId: any) => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    threshold = event.target.value as number;
    const data = [loggedUserId, threshold];
    axios.post('/api/user/profile/threshold', data);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  return (
    <div>
      <h3>Seuil en PM des notifications</h3>
      <FormControl className={classes.formControl}>
        <Select
          id='demo-controlled-open-select'
          open={open}
          onClose={handleClose}
          onOpen={handleOpen}
          value={threshold}
          onChange={handleChange}
        >
          <MenuItem value=''>
            <em style={{color: 'white'}}>None</em>
          </MenuItem>
          <MenuItem value={20} style={{color: 'white'}}>20 PM</MenuItem>
          <MenuItem value={25} style={{color: 'white'}}>25 PM</MenuItem>
          <MenuItem value={40} style={{color: 'white'}}>40 PM</MenuItem>
          <MenuItem value={80} style={{color: 'white'}}>80 PM</MenuItem>
        </Select>
      </FormControl>
    </div>
  );
};
