import * as Enzyme from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';
import * as React from 'react';
import {FacebookComment} from './FacebookComment';

describe('<FacebookComment/>', () => {
    beforeAll(() => Enzyme.configure({ adapter: new Adapter()}));
    it('should not throw error on build', () => {
        expect(() => Enzyme.shallow(<FacebookComment appId='' url=''/>)).not.toThrow();
    });
});