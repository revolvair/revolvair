import * as React from 'react';
import { useAuth0 } from '../../utils/hook';
import {ListItem, ListItemIcon, ListItemText} from '@material-ui/core';
import LockIcon from '@material-ui/icons/Lock';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';

export function Login() {
  const { loginWithRedirect, isAuthenticated, logout } = useAuth0();

  const ICON_WHITE_COLOR = 'secondary';

  // (e: React.MouseEvent)
  const loginHandler = async (e: any) => {
    e.preventDefault();
    if (loginWithRedirect) {
      await loginWithRedirect();
    }
  };

  return (
    <div className='button'>
        {!isAuthenticated && (
          <ListItem
              button
              onClick={loginHandler}>
              <ListItemIcon>
                  <LockIcon color={ICON_WHITE_COLOR}/>
              </ListItemIcon>
              <ListItemText primary='Connexion'/>
          </ListItem>
        )}

        {isAuthenticated &&
        <div>
          <ListItem
              button
              onClick={logout}>
              <ListItemIcon>
                  <ExitToAppIcon color={ICON_WHITE_COLOR}/>
              </ListItemIcon>
              <ListItemText primary='Déconnexion'/>
          </ListItem>
        </div>
        }
    </div>
  );
}
