import * as React from 'react';
import FavoriteIcon from '@material-ui/icons/Favorite';
import Button from '@material-ui/core/Button';
import axios from 'axios';

export interface UnlikeButtonProps<> {
    stationId: number;
    user: any;
}

export function UnlikeButton (props: UnlikeButtonProps) {
        return (
            <Button onClick={() => { removeFromFavorites(props.stationId, props.user.sub); }}>
                    <FavoriteIcon color='error'/>
            </Button>
        );
}

function removeFromFavorites(stationId: number, userId: any) {
    axios.delete('/api/users/' + userId + '/favorites/' + stationId);
}
