import * as React from 'react';
import Tooltip from '@material-ui/core/Tooltip/Tooltip';
import InfoIcon from '@material-ui/icons/InfoOutlined';
import IconButton from '@material-ui/core/IconButton/IconButton';

export interface InformationIconContainerProps {
    textBubble: string;
    onClick?: () => void;
}

export class InformationIcon extends React.Component<InformationIconContainerProps> {
    public render(): React.ReactNode {
        return (
            <div>
                <Tooltip title={this.props.textBubble} placement='right'>
                    <IconButton aria-label={this.props.textBubble} onClick={this.props.onClick}>
                        <InfoIcon color='secondary'/>
                    </IconButton>
                </Tooltip>
            </div>
        );
    }
}
