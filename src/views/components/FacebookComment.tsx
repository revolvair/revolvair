import * as React from 'react';
import { FacebookProvider, Comments } from 'react-facebook';

export interface FacebookCommentContainerProps {
    url: string;
    appId: string;
}

export class FacebookComment extends React.Component<FacebookCommentContainerProps> {

    constructor(props: FacebookCommentContainerProps) {
        super(props);
        this.state = {dataLoaded: false};
    }

    componentDidMount() {
        this.setState({dataLoaded: true});
    }

    public render(): React.ReactNode {
        return (
            <FacebookProvider appId={this.props.appId} language='fr_FR'>
                <Comments href={this.props.url} colorScheme='dark' />
            </FacebookProvider>
        );
    }
}
