import * as React from 'react';
import { Theme } from '@material-ui/core/styles';
import { createStyles, makeStyles } from '@material-ui/styles';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import axios from 'axios';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      minWidth: 420,
      color: 'white',
    },
    MenuItem: {
      minWidth: 420,
      color: 'white',
    },
  }),
);

let frequency = 30000; // Default value of Measure Frequency

export const MeasureFrequency = (loggedUserId: any) => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    frequency = event.target.value as number;
    const data = [loggedUserId, frequency];
    axios.post('/api/user/profile/frequency', data);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  return (
    <div>
      <h3>Fréquence de lecture des stations</h3>
      <FormControl className={classes.formControl}>
        <Select
          id='demo-controlled-open-select'
          open={open}
          onClose={handleClose}
          onOpen={handleOpen}
          value={frequency}
          onChange={handleChange}
        >
          <MenuItem value={15000}>
            <em style={{color: 'white'}}>None</em>
          </MenuItem>
          <MenuItem value={5000} style={{color: 'white'}}>5 Secondes</MenuItem>
          <MenuItem value={10000} style={{color: 'white'}}>10 Secondes</MenuItem>
          <MenuItem value={15000} style={{color: 'white'}}>15 Secondes</MenuItem>
          <MenuItem value={30000} style={{color: 'white'}}>30 Secondes</MenuItem>
        </Select>
      </FormControl>
    </div>
  );
};
