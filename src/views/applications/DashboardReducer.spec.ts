import {DashboardReducer, defaultDashboardState} from './DashboardReducer';
import {DashboardActions} from './DashboardActions';
import {Station} from '../../models/Station';

describe('DashboardReducer', () => {
    it('should return the initial state', () => {
        expect(DashboardReducer(undefined, {type: undefined})).toEqual(defaultDashboardState);
    });

    it('should handle UPDATE_STATION', () => {
        const stationToAdd: Station[] = [{id: 0, name: 'name', lat: 1, long: 1, sensors: []}];
        expect(DashboardReducer(
            undefined,
            {
                type: DashboardActions.UPDATE_STATIONS,
                stations: stationToAdd,
            },
        ).stations).toEqual(stationToAdd);
    });

    it('should handle CHANGE_DRAWER', () => {
        const drawerToAdd: boolean = true;
        expect(DashboardReducer(
            undefined,
            {
                type: DashboardActions.CHANGE_DRAWER,
                drawer: drawerToAdd,
            },
        ).drawer).toEqual(drawerToAdd);
    });
});
