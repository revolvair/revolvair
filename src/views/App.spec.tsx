import * as Enzyme from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';
import * as React from 'react';
import {App} from './App';

describe('<App/>', () => {
    beforeAll(() => Enzyme.configure({adapter: new Adapter()}));

    it('Should not throw error on build', () => {
        expect(() => Enzyme.shallow(<App/>)).not.toThrow();
    });
});
