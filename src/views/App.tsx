import * as React from 'react';
import {Route, RouteComponentProps, Redirect, Switch, withRouter} from 'react-router-dom';
import {DashboardConnected} from './applications/Dashboard';
import {StaticContext, Router} from 'react-router';
import { createBrowserHistory } from 'history';

export interface AppProps extends Readonly<RouteComponentProps<any, StaticContext, any>> {}

const history = createBrowserHistory();

class RevolvairApp extends React.Component<AppProps> {
    public render(): React.ReactNode {
        return (
            <Router history={history}>
                <Switch>
                    <Route
                        path='/dashboard'
                        render={props => (<DashboardConnected {...{location: props.location}}/>)}
                    />
                    <Redirect to='/dashboard'/>
                </Switch>
            </Router>
        );
    }
}

export const App = withRouter(RevolvairApp);
