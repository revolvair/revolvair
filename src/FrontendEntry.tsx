import {createMuiTheme, MuiThemeProvider} from '@material-ui/core/styles';
import * as moment from 'moment';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {BrowserRouter} from 'react-router-dom';
import {createStore} from 'redux';
import {App} from './views/App';
import {revolvairReducer} from './views/AppReducer';
import createBreakpoints, {Breakpoints} from '@material-ui/core/styles/createBreakpoints';
import * as dotenv from 'dotenv';

import {Auth0Provider} from './react-auth0-spa';
import { createBrowserHistory } from 'history';

dotenv.config();
moment.locale('fr');
dotenv.config();

const store = createStore(revolvairReducer);
const whiteColor = '#FFF';
const darkColor = '#02121B';
const darkBlueColor = '#0A3145';
const lightBlue = '#13445C';

const breakpoints: Breakpoints = createBreakpoints({});

const theme = createMuiTheme({
    palette: {
        primary: {main: darkColor},
        secondary: {main: whiteColor},
    },
    typography: {
        fontFamily: 'Montserrat, sans-serif',
        useNextVariants: true,
    },
    overrides: {
        MuiTypography: {
            h3: {
                fontSize: '3.5rem',
                [breakpoints.down('xs')]: {
                    fontSize: '1.2rem',
                },
            },
            h4: {
                fontSize: '2.5rem',
                [breakpoints.down('xs')]: {
                    fontSize: '1rem',
                },
            },
            body1: {
                color: whiteColor,
                fontSize: '1.2rem',
                [breakpoints.down('xs')]: {
                    fontSize: '0.8rem',
                },
            },
        },
        MuiPaper: {
            root: {
                backgroundColor: darkColor,
                overflow: 'auto',
            },
        },
        MuiTableHead: {
            root: {
                backgroundColor: darkColor,
                position: 'sticky',
                top: 0,
                zIndex: 10,
            },
        },
        MuiTableCell: {
            root: {
                borderBottom: `1px solid ${darkBlueColor}`,
            },
            body: {
                color: whiteColor,
            },
            head: {
                color: whiteColor,
            },
        },
        MuiTableSortLabel: {
            root: {
                '&:hover': {
                    color: whiteColor,
                },
                '&:focus': {
                    color: whiteColor,
                },
            },
            active: {
                color: whiteColor,
            },
            icon: {
                color: whiteColor,
            },
        },
        MuiFormLabel: {
             root: {
                color: whiteColor,
                '&$focused': {
                    color: whiteColor,
                },
             },
        },
        MuiInput: {
            underline: {
                '&:before': {
                    borderBottomColor: whiteColor,
                    color: whiteColor,
                },
                '&:after': {
                    color: whiteColor,
                    borderBottomColor: whiteColor,
                },
                '&&&&:hover:not($disabled):before': {
                    borderBottom: '2px solid rgba(255, 255, 255, 1)',
                },
            },
        },
        MuiInputBase: {
            root: {
                width: 200,
                color: whiteColor,
            },
        },
        MuiButton: {
            root: {
                width: 200,
            },
        },
        MuiListItem: {
            button: {
                '&:hover': {
                    backgroundColor: lightBlue,
                },
            },
        },
        MuiBadge: {
            badge: {
                top: '50%',
                left: '50px',
            },
        },
    },
});

let APP_URL = 'https://revolvair.ml/dashboard/home';
if (window.location.hostname == 'localhost') {
    APP_URL = 'http://localhost:80/dashboard/home';
}

const onRedirectCallback = (appState?: any) => {
    const history = createBrowserHistory();
    history.push(
      appState && appState.targetUrl
        ? appState.targetUrl
        : window.location.pathname,
    );
  };

document.addEventListener('DOMContentLoaded', () => {

    render((
            <Provider store={store}>
                <MuiThemeProvider theme={theme}>
                    <BrowserRouter>
                        <Auth0Provider
                            // .env variables unavailable in FrontendEntry.tsx
                            domain={'dev-taq03kgf.auth0.com'}
                            client_id={'y9cqJdMUxG2oLsrY9JRvKs66uTug70gA'}
                            redirect_uri={APP_URL}
                            onRedirectCallback={onRedirectCallback}
                            audience={'https://revolvair.ca'}
                        >
                                <App/>
                        </Auth0Provider>
                    </BrowserRouter>
                </MuiThemeProvider>
            </Provider>
    ));
});
const render = (element: React.ReactElement<any>) => ReactDOM.render(
element, document.querySelector('#reactroot'));
