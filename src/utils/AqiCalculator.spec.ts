// import {AdminPageActions, updateToken} from './AdminPageActions';
import {AqiCalculator} from './AqiCalculator';

describe('AqiCalculator', () => {
    describe('calculatePM25 with 25', () => {
        it('Should return the good concentration', () => {
            const ANY_NUMBER = 25;
            const EXPECTED_CONCENTRATION = 6;
            const calc = new AqiCalculator();
            var conc = calc.ConcentrationPM25(ANY_NUMBER);
            conc = +((Math.round(conc * 100) / 100).toFixed(1));
            expect(conc).toEqual(EXPECTED_CONCENTRATION);
        });
    });

    describe('calculatePM25 with 75', () => {
        it('Should return the good concentration', () => {
            const ANY_NUMBER = 75;
            const EXPECTED_CONCENTRATION = 23.5;
            const calc = new AqiCalculator();
            var conc = calc.ConcentrationPM25(ANY_NUMBER);
            conc = +((Math.round(conc * 100) / 100).toFixed(1));
            expect(conc).toEqual(EXPECTED_CONCENTRATION);
        });
    });

    describe('calculatePM25 with 125', () => {
        it('Should return the good concentration', () => {
            const ANY_NUMBER = 125;
            const EXPECTED_CONCENTRATION = 45.3;
            const calc = new AqiCalculator();
            var conc = calc.ConcentrationPM25(ANY_NUMBER);
            conc = +((Math.round(conc * 100) / 100).toFixed(1));
            expect(conc).toEqual(EXPECTED_CONCENTRATION);
        });
    });

    describe('calculatePM25 with 175', () => {
        it('Should return the good concentration', () => {
            const ANY_NUMBER = 175;
            const EXPECTED_CONCENTRATION = 102;
            const calc = new AqiCalculator();
            var conc = calc.ConcentrationPM25(ANY_NUMBER);
            conc = +((Math.round(conc * 100) / 100).toFixed(1));
            expect(conc).toEqual(EXPECTED_CONCENTRATION);
        });
    });

    describe('calculatePM25 with 250', () => {
        it('Should return the good concentration', () => {
            const ANY_NUMBER = 250;
            const EXPECTED_CONCENTRATION = 199.9;
            const calc = new AqiCalculator();
            var conc = calc.ConcentrationPM25(ANY_NUMBER);
            conc = +((Math.round(conc * 100) / 100).toFixed(1));
            expect(conc).toEqual(EXPECTED_CONCENTRATION);
        });
    });

    describe('calculatePM25 with 350', () => {
        it('Should return the good concentration', () => {
            const ANY_NUMBER = 350;
            const EXPECTED_CONCENTRATION = 299.9;
            const calc = new AqiCalculator();
            var conc = calc.ConcentrationPM25(ANY_NUMBER);
            conc = +((Math.round(conc * 100) / 100).toFixed(1));
            expect(conc).toEqual(EXPECTED_CONCENTRATION);
        });
    });

    describe('calculatePM25 with 450', () => {
        it('Should return the good concentration', () => {
            const ANY_NUMBER = 450;
            const EXPECTED_CONCENTRATION = 424.7;
            const calc = new AqiCalculator();
            var conc = calc.ConcentrationPM25(ANY_NUMBER);
            conc = +((Math.round(conc * 100) / 100).toFixed(1));
            expect(conc).toEqual(EXPECTED_CONCENTRATION);
        });
    });

    describe('calculatePM25 with error in value', () => {
        it('Should return the error value', () => {
            const ANY_NUMBER = -100;
            const EXPECTED_CONCENTRATION = -1;
            const calc = new AqiCalculator();
            var conc = calc.ConcentrationPM25(ANY_NUMBER);
            expect(conc).toEqual(EXPECTED_CONCENTRATION);
        });
    });
});

