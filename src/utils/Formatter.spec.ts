import * as moment from 'moment';
import {ImportantValues} from '../views/components/Graph';
import {Formatter} from './Formatter';
import {hoursInDay, timestampTest, createMoment} from './SpecConstants';
import {Utils} from './Utils';

describe('Formatter',  () => {
    let now: moment.Moment;
    beforeEach(() => now = createMoment(timestampTest));
    describe('formatImportantValuesFromGraphData', () => {
        const expectedImportantValues: ImportantValues = {
            max: undefined,
            avg: undefined,
            min: undefined,
        };
        it('Should return all attrs as undefined if no data are passed', () => {
            const importantValues: ImportantValues = Formatter.formatImportantValuesFromGraphData([]);

            const expected: ImportantValues = expectedImportantValues;
            expect(importantValues).toEqual(expected);
        });

        it('Should return max as 4 if null, 2 and 4 are passed', () => {
            const importantValues: ImportantValues = Formatter.formatImportantValuesFromGraphData([
                {min: null, avg: null, max: null},
                {min: null, avg: null, max: 4},
                {min: null, avg: null, max: 2},
            ]);

            const expected: ImportantValues = {
                ...expectedImportantValues,
                max: 4,
            };
            expect(importantValues).toEqual(expected);
        });

        it('Should return min as 2 if null, 2 and 4 are passed', () => {
            const importantValues: ImportantValues = Formatter.formatImportantValuesFromGraphData([
                {min: null, avg: null, max: null},
                {min: 4, avg: null, max: null},
                {min: 2, avg: null, max: null},
            ]);

            const expected: ImportantValues = {
                ...expectedImportantValues,
                min: 2,
            };
            expect(importantValues).toEqual(expected);
        });

        it('Should return avg as 2 if null, 1, 2 and 3 are passed', () => {
            const importantValues: ImportantValues = Formatter.formatImportantValuesFromGraphData([
                {min: null, avg: null, max: null},
                {min: null, avg: 3, max: null},
                {min: null, avg: 2, max: null},
                {min: null, avg: 1, max: null},
            ]);

            const expected: ImportantValues = {
                ...expectedImportantValues,
                avg: 2,
            };
            expect(importantValues).toEqual(expected);
        });
    });

    describe('formatMeasures', () => {
        it('Should return an empty if an empty array passed', () => {
            expect(Formatter.formatMeasures([])).toEqual([]);
        });

        it('Should return a value if an array passed', () => {
            const nowTest = Utils.createMoment(timestampTest);
            expect(Formatter.formatMeasures([{timestamp: timestampTest.toString(), value: 4}])).toEqual([{timestamp: nowTest, value: 4}]);
        });
    });

    describe ('formatGraphData', () => {
        it('Should return all hours but no data if no data are passed', () => {
            expect(Formatter.formatGraphData(Utils.TimeGap.Day, [], 'H[h]', now)).toEqual(hoursInDay.map((name => ({name, [Formatter.DataKey]: undefined}))));
        });

        it('Should return data with tooltip if no data are passed', () => {
            const date = createMoment(now.unix() * 1000).endOf('day');
            const graphData = Formatter.formatGraphData(Utils.TimeGap.Day, [{value: 5, timestamp: createMoment(now.unix() * 1000).subtract(2, 'hour')}], 'H', date, true);
            expect(graphData).toEqual(Utils.getDatesIn(Utils.TimeGap.Day, date, 'H').map(name => ((name === '3')
                ? {name, [Formatter.DataKey]: 5, tooltip: {max: 5, min: 5, avg: 5}}
                : {name, [Formatter.DataKey]: undefined})));
        });

        it('Should return data with tooltip if no data are passed', () => {
            const date = createMoment(now.unix() * 1000).endOf('day');
            const graphData = Formatter.formatGraphData(Utils.TimeGap.Day, [{value: 5, timestamp: createMoment(now.unix() * 1000).subtract(2, 'hour')}], 'H', date);
            expect(graphData).toEqual(Utils.getDatesIn(Utils.TimeGap.Day, date, 'H').map(name => ((name === '3')
                ? {name, [Formatter.DataKey]: 5, tooltip: undefined}
                : {name, [Formatter.DataKey]: undefined})));
        });
    });

    describe('mergeMeasures', () => {
        it('Should add an new name if does not allready exist', () => {
            const mergedMeasures = Formatter.mergeMeasures([{name: '6h', values: undefined}], [{value: 3, timestamp: now}], 'H[h]');

            expect(mergedMeasures).toEqual([
                {name: '6h', values: undefined},
                {name: '5h', values: [3]},
            ]);
        });

        it('Should add an new value to array if array allready exist', () => {
            const mergedMeasures = Formatter.mergeMeasures([{name: '5h', values: []}], [{value: 3, timestamp: now}], 'H[h]');

            expect(mergedMeasures).toEqual([
                {name: '5h', values: [3]},
            ]);
        });

        it('Should add an new value and array if array does not allready exist', () => {
            const mergedMeasures = Formatter.mergeMeasures([{name: '5h', values: undefined}], [{value: 3, timestamp: now}], 'H[h]');

            expect(mergedMeasures).toEqual([
                {name: '5h', values: [3]},
            ]);
        });
    });
});
