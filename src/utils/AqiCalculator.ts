
export class AqiCalculator {

    private InvLinear(AQIhigh: number, AQIlow: number, Conchigh: number, Conclow: number, conc: number) {
        let c;
        c = ((conc - AQIlow) / (AQIhigh - AQIlow)) * (Conchigh - Conclow) + Conclow;
        return c;
    }

    ConcentrationPM25(conc: number) {

        let ConcCalculated;

        if (conc >= 0 && conc <= 50) {
            ConcCalculated = this.InvLinear(50, 0, 12, 0, conc);
        } else if (conc > 50 && conc <= 100) {
            ConcCalculated = this.InvLinear(100, 51, 35.4, 12.1, conc);
        } else if (conc > 100 && conc <= 150) {
            ConcCalculated = this.InvLinear(150, 101, 55.4, 35.5, conc);
        } else if (conc > 150 && conc <= 200) {
            ConcCalculated = this.InvLinear(200, 151, 150.4, 55.5, conc);
        } else if (conc > 200 && conc <= 300) {
            ConcCalculated = this.InvLinear(300, 201, 250.4, 150.5, conc);
        } else if (conc > 300 && conc <= 400) {
            ConcCalculated = this.InvLinear(400, 301, 350.4, 250.5, conc);
        } else if (conc > 400 && conc <= 500) {
            ConcCalculated = this.InvLinear(500, 401, 500.4, 350.5, conc);
        } else {
            ConcCalculated = -1;
        }
        return ConcCalculated;
    }
}

/*
function ConcPM10(a)
{
	if (a>=0 && a<=50)
	{
		ConcCalc=InvLinear(50,0,54,0,a);
	}
	else if (a>50 && a<=100)
	{
		ConcCalc=InvLinear(100,51,154,55,a);
	}
	else if (a>100 && a<=150)
	{
		ConcCalc=InvLinear(150,101,254,155,a);
	}
	else if (a>150 && a<=200)
	{
		ConcCalc=InvLinear(200,151,354,255,a);
	}
	else if (a>200 && a<=300)
	{
		ConcCalc=InvLinear(300,201,424,355,a);
	}
	else if (a>300 && a<=400)
	{
		ConcCalc=InvLinear(400,301,504,425,a);
	}
	else if (a>400 && a<=500)
	{
		ConcCalc=InvLinear(500,401,604,505,a);
	}
	else
	{
		ConcCalc="PM10message";
	}
	return ConcCalc;
}
*/
