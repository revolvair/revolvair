import { useContext } from 'react';
import { Auth0Context } from '../react-auth0-spa';
export const useAuth0 = () => useContext(Auth0Context)!;
