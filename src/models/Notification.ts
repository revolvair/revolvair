export interface Notification {
    stationId: number;
    oldValue: number;
    newValue: number;
    timestamp: string;
    oldState: string;
    newState: string;
}
