export interface Measure {
    sensorId?: number;
    timestamp: string;
    value: number;
}
