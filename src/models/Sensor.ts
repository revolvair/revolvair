import {Measure} from './Measure';

export interface Sensor {
    id: number;
    sensorType: number;
    stationId: number;
    measures?: Measure[];
    latestMeasures?: Measure[];
    max?: number;
    min?: number;
    avg?: number;
}
