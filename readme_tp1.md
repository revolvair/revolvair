## Mise en place rapide :
1.  Installer :
    * -Curl 
	* -NodeJs
	* -git
	* -ruby
	* -Docker & docker-compose
2. Suivre le wiki : gitlab.com/revolvair/revolvair/-/wikis/home
> En local, nous pouvons quand meme seeder les données:
>```
>$ bash elastic.sh
>$ ./seedElastic.rb --create-index --seed stations,notifications,measures -u http://localhost:9200
>```

### **Noms** : *Jonathan Houle, Mathieu Tremblay, Émile Reny-Déry, Jonathan Deshaies*

Afin d'utiliser l'application, il suffit de cloner le projet sur gitlab :
	gitlab.com/revolvair/revolvair
et de la lancer selon les besoins; *en local sans docker, ou avec docker*

## Paragraphes de développement 

* Mathieu Tremblay :
	Je me suis occupé en majeure partie de l'intégration continue de l'environnement test et staging dans gitlab. 
	Tout ce qui concerne le container registry également qui sert à emmagasiner nos conteneurs docker et les images qu'ils contiennent. 
	Je me suis aussi occupé du fichier de configuration gitlab CI.
	Aider Émile et Jonathan Houle avec leur installation de l'environnement & régler de certains problèmes reliés avec NPM.

* Jonathan Houle :
	Faire les US et les reformuler selon ceux de Guillaume Simard.
	Avec Mathieu : Faire le commencement du pipeline Gitlab CI.
	Avec Émile : L'aider a faire le script d'installation de départ.

* Jonathan Deshaies :
	Faire fonctionner la carte Google Maps de l'application en changeant la clé d'API.
	Avec Monsieur Reny-Déry, mise en place de l'application ainsi que la BD Elastic Search sur AWS.

* Émile RDéry :
	Remplissage du rapport technique pour la remise du TP1
	Création d'un script afin d'installer les dépendances nécessaires au bon fonctionnement de l'application
	avec Jonathan Houle : Ajouter / refaire / reformuler les différents US
	avec Jonathan Deshaies : Nous avons regardés pour la migrations des données sur AWS avec l'application.
