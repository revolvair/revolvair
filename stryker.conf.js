module.exports = function(config) {
  config.set({
    mutator: "typescript",
    // logLevel: 'trace', // to debug
    packageManager: "npm",
    reporters: ["html", "clear-text", "progress"],
    testRunner: "jest",
    transpilers: ["typescript"],
    coverageAnalysis: "off",
    tsconfigFile: "tsconfig.json",
    mutate: [
      "./src/**/*.ts",
      "./src/**/*.tsx",
      "!./src/FrontendEntry.tsx",
      "!./src/ServerEntry.tsx",
      "!./src/**/*.spec.ts",
      "!./src/**/*.spec.tsx",
    ],
    transform: {
      "^.+\\.tsx?$": "ts-jest",
    },
  });
};
